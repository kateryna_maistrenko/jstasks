/**
 * this function is used in different tasks*/

function countArraySum(arr) {
    return arr.reduce((sum, elem) => {
        return sum + elem;
    }, 0);
}


/** Large summation */

// The first solution

/**
 * return the multiples of a number which are less then max */

function getMultiples(max, num) {
    let multiples = [];
    const maxNum = max / num;
    for (let i = 1; i < maxNum; i++) {
        multiples.push(num * i);
    }
    return multiples;
}

/**
 * return sum of the multiples of numbers which are less then max */

function countLargeSummation(max, ...numbers) {
    try {
        const multiples = numbers.flatMap(number => {
            return getMultiples(max, number);
        });

        // get only unique multiples for all numbers
        const uniqueMultiples = Array.from(new Set(multiples));

        return countArraySum(uniqueMultiples);
    } catch (e) {
        console.log(e);
    }
}
let time = performance.now();
// console.log(countLargeSummation(1000000, 5, 7));
// time = performance.now() - time;
// console.log('Время выполнения = ', time);




// The second solution

function countSummation(max) {
    const firstNumber = 5;
    const secondNumber = 7;
    let sum = 0;
    
    while(max !== 0) {
        max--;
        if (max % firstNumber === 0 || max % secondNumber === 0) {
            sum += max;
        }
    }
    return sum;
}

let time1 = performance.now();
// console.log(countSummation(1000000));
// time1 = performance.now() - time1;
// console.log('Время выполнения = ', time1);




// The third solution


function countSum(max) {
    const firstNumber = 5;
    const secondNumber = 7;
    const arr = new Array(max).fill(0);
    return arr.reduce((sum, num, idx) => {
        if (idx % firstNumber === 0 || idx % secondNumber === 0) {
            sum += idx;
        }
        return sum
    }, 0);
}
let time2 = performance.now();
// console.log(countSum(1000000));
// time2 = performance.now() - time2;
// console.log('Время выполнения = ', time2);


// The LAST SOLUTION

function getLastMultipler(n, max) {
    return Math.floor((max - 1) / n)
}

/**
 * return the sum of numbers of arithmetic progression from first to last specified
 */
function getSumOfArithmeticProgression(num, max) {
    const amount = getLastMultipler(num, max);
    // arithmetic formula
    return amount / 2 * (2 * num + (amount - 1) * num);
}

function countProgression(max) {
    const firstNumber = 5;
    const secondNumber = 7;

    // collect numbers and their multiplication into array to avoid code duplication
    const numbers = [firstNumber, secondNumber, (firstNumber * secondNumber)];

    // count sums of multiples for each number and their multiplication
    [firstSum, secondSum, bothSum] = numbers.map(number => {
        return getSumOfArithmeticProgression(number, max);
    });

    // sum of all multiples is sum of all multiples of each number except common multiples
    return firstSum + secondSum - bothSum;
}

let time3 = performance.now();
// console.log('sum ', countProgression(1000000));
// time3 = performance.now() - time3;
// console.log('Время выполнения = ', time3);


/** Recursive y-defined sequence */

/**
 * return an element of Recursive y-defined sequence*/
function f(n) {
    if (n % 2 === 0) {
        return n / 2;
    } else {
        return (1 + 3 * n);
    }
}

/**
 * return the array of Recursive y-defined sequence */

function a(n) {
    const arr = [];
    try {
        arr.push(n);
        while (n !== 1) {
            const newValue = f(n);
            arr.push(newValue);
            n = newValue;
        }
        return arr;
    } catch (e) {
        console.log(e);
    }

}

// Count the sum of array of Recursive y-defined sequence
// console.log(countArraySum(a(6171)));


/** Leap day */


/**
 * return boolean; if the year has a leap day return true, otherwise return false */

function hasLeapDay(year) {
    return (year % 400 === 0 || year % 4 === 0 && year % 100 !== 0);
}

/**
 * return a numeric weekday */

function getWeekday(year, month, date) {
    const weekday = new Date(year, month, date);
    return weekday.getDay();
}

/**
 * return an object where keys are numeric weekdays, values are amount of weekdays */

function countLeapDaysByWeekdays(min, max) {

    const leapDays = {
        '0': 0,
        '1': 0,
        '2': 0,
        '3': 0,
        '4': 0,
        '5': 0,
        '6': 0
    };
    const date = 29;
    const month = 1;

    let year = min;
    try {
        while (year <= max) {
            if (hasLeapDay(year)) {
                const weekday = getWeekday(year, month, date);
                leapDays[weekday] += 1;
            }
            year++;
        }
        return leapDays;
    } catch (e) {
        console.log(e);
    }
}

/**
 * return prettified object where weekdays are strings*/

function transformLeapWeekdaysToStr(leapDays) {

    const map = {
        0: 'Sunday',
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday',
        6: 'Saturday'
    };
    const result = {};

    for (let day in leapDays) {
        if (leapDays.hasOwnProperty(day)) {
            result[map[day]] = leapDays[day];
        }
    }
    return result;
}

// console.log(transformLeapWeekdaysToStr(countLeapDaysByWeekdays(1900, 2000)));

const matrix = [
    [1, 2, 3, 4, 5],
    [2, 4, 6, 8, 10],
    [3, 6, 9, 12, 15],
    [4, 8, 12, 16, 20],
    [5, 10, 15, 20, 25]
];

for (let i = 0; i < matrix.length; i++) {
    console.log(matrix[i][i]);
}
